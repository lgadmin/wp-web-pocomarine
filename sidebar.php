<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<aside role="complementary">
				<?php dynamic_sidebar( 'primary-widget-area' ); ?>
		</aside>