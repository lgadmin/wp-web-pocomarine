<?php

function child_twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Sidebar Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Top Navigation', 'twentyten' ),
		'id' => 'topnav-widget-area',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Area', 'twentyten' ),
		'id' => 'footer-widget-area',
		'description' => __( 'These widgets will show up on your footer', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
}
//AFTER THEME IS SET UP (FUNCTIONS.PHP IS LOADED FROM PARENT THEME) - REMOVE THE SIDEBAR WIDGETS
add_action('after_setup_theme','remove_parent_widgets');
function remove_parent_widgets() {
    remove_action( 'widgets_init', 'twentyten_widgets_init' );
}
//RE-ADD THE SIDEBAR WIDGETS WITH THE NEW CODE ADDED, DUPLICATING THE FUNCTION
add_action( 'after_setup_theme', 'child_twentyten_widgets_init' );

//ADD REALLY SIMPLE CAPTCHA BACK TO CONTACT FORM 7
add_filter( 'wpcf7_use_really_simple_captcha', '__return_true' );

//ADD A NEW MENU LOCATION
function register_my_menus() {
  register_nav_menus(
    array(  
    	'footer-nav' => __( 'Footer Menu' ), 
    )
  );
} 
add_action( 'init', 'register_my_menus' );


//REMOVE RELATED PRODUCTS
function wc_remove_related_products( $args ) {
return array();
}
add_filter('woocommerce_related_products_args','wc_remove_related_products', 10);

//ADD CALL FOR PRICE
add_filter('woocommerce_empty_price_html', 'custom_call_for_price');
function custom_call_for_price() {
     return '<a class="button product_type_simple call_for_pricing" href="tel:604-464-8773">Call for Pricing</a>';
}

//REMOVE REVIEWS
add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {
 unset($tabs['reviews']);
 return $tabs;
}


// SHOW 20 ITEMS PER PAGE
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 20;' ), 20 );


/**
 * Disable free shipping for select products
 *
 * @param bool $is_available
 */
//function my_free_shipping2( $is_available ) {
//	global $woocommerce;
//	$ineligible = array( '4616', '14031' );
//	$cart_items = $woocommerce->cart->get_cart();
//	foreach ( $cart_items as $key => $item ) {
//		if( in_array( $item['product_id'], $ineligible ) ) {
//			return false;
//		}
//	}
//	return $is_available;
//}
//add_filter( 'woocommerce_shipping_free_shipping_is_available', 'my_free_shipping2', 20 );


// show product dimensions in admin area
add_filter('manage_product_posts_columns', 'my_manage_product_posts_columns', 10);
add_action('manage_product_posts_custom_column', 'my_manage_product_posts_custom_column', 10, 2);

function my_manage_product_posts_columns($defaults) {
    $defaults['weight'] = 'Weight';
    return $defaults;
}
function my_manage_product_posts_custom_column($column_name, $post_ID) {
    if ($column_name == 'weight') {
        $p = new WC_Product($post_ID);
        echo $p->get_weight();
    }
}




/**
 * @snippet       Disable Free Shipping if Cart has Shipping Class (WooCommerce 2.6+)
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=19960
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 2.6.1
 */
 
add_filter( 'woocommerce_package_rates', 'businessbloomer_hide_free_shipping_for_shipping_class', 10, 2 );
  
function businessbloomer_hide_free_shipping_for_shipping_class( $rates, $package ) {
$shipping_class_target = 220; // shipping class ID (to find it, see screenshot below)
$in_cart = false;
foreach( WC()->cart->cart_contents as $key => $values ) {
 if( $values[ 'data' ]->get_shipping_class_id() == $shipping_class_target ) {
  $in_cart = true;
  break;
 } 
}
if( $in_cart ) {
 unset( $rates['free_shipping:2'] ); // shipping method with ID (to find it, see screenshot below)
}
return $rates;
}

?>