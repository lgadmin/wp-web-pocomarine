<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
    <footer class="fullwidth" role="contentinfo">
        <div class="container">
          <?php dynamic_sidebar( 'footer-widget-area' ); ?>
        </div>
        
  <div class="footbar fullwidth">
      <div class="container">
                <div id="copyright">
                    <?php
	get_sidebar( 'footer' );
	$date = getdate();
	$year = $date['year']; 
?> &copy; Copyright <?php echo("$year"); ?> <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div><div id="longevity"> <a target="_blank" href="http://www.longevitygraphics.com">Website Design</a> by <a target="_blank" href="http://www.longevitygraphics.com">Longevity Graphics</a> </div>
      </div>  
  </div>
 
    </footer>
  <?php wp_footer(); ?>
</body>
</html>