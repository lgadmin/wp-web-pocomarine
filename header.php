<?php
/**
 * The Header for our child theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Responsive Twenty_Ten
 * @since Responsive Twenty Ten 0.1
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<meta name="google-site-verification" content="JfB_N9gw9LthhpYJ15_3AoXrRaRqBOrc_dl37RZ6H_o" />
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/font-awesome.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/css_browser_selector.min.js"></script>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
?>
<?php wp_head(); ?>

<script>
$(document).ready(function () {

//ADD RESPONSIVE TAG TO ALL TABLES IN PRODUCT DESCRIPTION
$('table').closest('#tab-description').find('table').addClass('responsive'); 

//JS FOR RESPONSIVE TABLES
var tables = document.querySelectorAll(".responsive"),
    tbody, headers, headertext, i, j, k;

for (i = 0; i < tables.length; i++) {

    tbody = tables[i].tBodies[0];
    headers = tables[i].tHead.rows[0].children;
    headertext = [];

    for (j = 0; j < headers.length; j++) {
        headertext.push(headers[j].textContent.trim());
    }

    for (j = 0; j < tbody.rows.length; j++) {
        for (k = 0; k < tbody.rows[j].cells.length; k++) {
            tbody.rows[j].cells[k].setAttribute("data-th", headertext[k]);
        }
    }    
}

});
</script>

<script>
$(document).ready(function () {


    if ($('.wmcs_currency_switcher_dropdown option:selected').text() == 'CAD($)'){
      //alert($('.wmcs_currency_switcher_dropdown option:selected').text());
      $('.curflag').css('background-image','url(https://www.pocomarine.com/wp-content/themes/pmarine/images/CA.png)');
    }else{
      //alert($('.wmcs_currency_switcher_dropdown option:selected').text());
      $('.curflag').css('background-image','url(https://www.pocomarine.com/wp-content/themes/pmarine/images/US.png)');
    }
  

  //$('select[name=Mounted]').change(function(e){
  //  if ($('select[name=Mounted]').val() == 'Fascia'){
  //    $('#FasciaYes').show();
  //  }else{
  //    $('#FasciaYes').hide();
  //  }
  //});


});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27146600-1', 'auto');
  ga('send', 'pageview');
</script> 
</head>

<body <?php body_class(); ?>>

<div id="topnav" class="fullwidth">
	<div class="container">
        <?php //wp_nav_menu( array( 'theme_location' => 'max_mega_menu_1' ) ); ?>
	<?php dynamic_sidebar( 'topnav-widget-area' ); ?>
	</div>
</div>

<header class="fullwidth" role="banner">
  <div class="container">

    <a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
       <?php if(is_front_page()){?>
       <h1><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></h1>
       <?php } ?>
       <img class="logo" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" />
    </a>
       
<div id="headright">
  <!--  woocommerce shopping cart --> 
  <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
  $count = WC()->cart->cart_contents_count; ?>
  <a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php if ( $count > 0 ) echo '<span class="countNumber">'. $count . '</span>'; ?></a>
  <?php } ?>
  <!--  woocommerce shopping cart ends --> 

  <?php echo do_shortcode('[yith_woocommerce_ajax_search]'); ?> 
</div>

  </div>
</header>

<nav role="navigation" class="fullwidth">
  <div class="container">
    <div class="skip-link screen-reader-text"> <a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>">
      <?php _e( 'Skip to content', 'twentyten' ); ?>
      </a> </div>
    <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
  </div>
</nav>

<?php putRevSlider('homepage2', 'homepage'); ?>

<div class="breadcrumbs fullwidth" typeof="BreadcrumbList" vocab="http://schema.org/">
	<div class="container">
		<?php if(function_exists('bcn_display')){ bcn_display(); }?>
    </div>
</div>